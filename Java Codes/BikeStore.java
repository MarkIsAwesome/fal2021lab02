//Mark Agluba
//1533777

public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bikes = new Bicycle[4];
        bikes[0]=new Bicycle("Trek Bikes", 5,30);
        bikes[1]=new Bicycle("Giant Bicycles", 7, 40);
        bikes[2]=new Bicycle("SBC", 3, 20);
        bikes[3]=new Bicycle("RedLine", 8, 50);

        for(int i=0;i<bikes.length;i++) {
            System.out.println(bikes[i]);
        }
    }
}
